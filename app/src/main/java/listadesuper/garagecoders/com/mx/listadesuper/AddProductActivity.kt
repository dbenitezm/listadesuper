package listadesuper.garagecoders.com.mx.listadesuper

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_add_product.*
import android.app.Activity
import android.content.Intent
import android.support.design.widget.Snackbar
import listadesuper.garagecoders.com.mx.listadesuper.models.Producto
import listadesuper.garagecoders.com.mx.listadesuper.utils.Constants
import listadesuper.garagecoders.com.mx.listadesuper.utils.SharedListManagement
import mx.com.ia.comunicacioninterna.SharedPreferencesManager


class AddProductActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)

        this.button_accept.setOnClickListener {
            if(validate(this.edit_text_id.text.toString())) {
                val returnIntent = Intent()
                val product = Producto(
                        this.edit_text_id.text.toString(),
                        this.edit_text_name.text.toString(),
                        this.edit_text_details.text.toString(), "")
                SharedListManagement.addProduct(product)
                var listProduct = SharedPreferencesManager.getProducts(this)
                listProduct.add(product)
                SharedPreferencesManager.saveProduct(this, listProduct)
                setResult(Constants.PICK_PRODUCT_REQUEST, returnIntent)
                finish()
            }else{
                Snackbar.make(it, "Id ${this.edit_text_id.text.toString()} ya existe", Snackbar.LENGTH_LONG)
                        .setAction("Aceptar", null).show()
            }
        }
    }


    fun validate(id: String): Boolean{
        val list=SharedListManagement.getListProduct()
        var i=0
        val sizelist=list.size
        while(i<sizelist){
            if(list[i].id==id){
                return false
            }
            i++
        }
        return true
    }
}
