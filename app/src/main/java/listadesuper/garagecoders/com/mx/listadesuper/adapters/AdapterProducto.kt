package listadesuper.garagecoders.com.mx.listadesuper.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.item_producto.view.*
import listadesuper.garagecoders.com.mx.listadesuper.R
import listadesuper.garagecoders.com.mx.listadesuper.models.Producto

class AdapterProducto(context: Context): RecyclerView.Adapter<AdapterProducto.ViewHolder>() {

    private val context=context
    private var list: MutableList<Producto> = mutableListOf()
    private var inflater : LayoutInflater?= null
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        if(this.inflater===null){
            inflater= LayoutInflater.from(parent.context)
        }
        val view =inflater!!.inflate(R.layout.item_producto,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int =list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.name.text=list[position].name
        holder?.details.text=list[position].details
        holder.itemView.setOnClickListener{
            val builder = AlertDialog.Builder(this.context)
            builder.setMessage(R.string.message_dialog_buy)
                    .setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, id ->
                        it.text_view_buy.visibility=View.VISIBLE
                    })
                    .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id ->

                    })
                    .create()
                    .show()
        }
    }

    fun update(list:List<Producto>){
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int){

    }


    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val name: TextView =view.findViewById(R.id.text_view_name)
        val details: TextView= view.findViewById(R.id.text_view_details)
        val image: ImageView= view.findViewById(R.id.image_view_producto)
    }

}