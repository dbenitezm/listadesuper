package mx.com.ia.comunicacioninterna

import android.content.Context
import android.content.SharedPreferences
import android.text.Editable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import listadesuper.garagecoders.com.mx.listadesuper.models.Producto
import listadesuper.garagecoders.com.mx.listadesuper.utils.Constants

object SharedPreferencesManager {

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(Constants.GLOBAL_PREFENRES, Context.MODE_PRIVATE)
    }

    fun getToken(context: Context): String {
        return getSharedPreferences(context).getString("", "") ?: ""
    }


    fun saveProduct(context: Context, products: MutableList<Producto>) {
        val editor = getSharedPreferences(context).edit()
        val gson=Gson()
        editor?.putString(Constants.KEY_LIST_PRODUCT, gson.toJson(products))
        editor?.apply()
    }

    fun deleteProduct(context: Context,product: Producto){
        var products= getProducts(context)
        if (products.isNotEmpty()){
            products.remove(product)
            saveProduct(context,products)
        }
    }

    fun getProducts(context: Context) : MutableList<Producto>{
        var list: MutableList<Producto> = mutableListOf()
        if(getSharedPreferences(context).contains(Constants.KEY_LIST_PRODUCT)){
            val products= getSharedPreferences(context).getString(Constants.KEY_LIST_PRODUCT,"")
            list=Gson().fromJson(products,object: TypeToken<MutableList<Producto>>(){}.type)
        }
        return list
    }

    fun clearUserData(context: Context){
        val editor = getSharedPreferences(context)!!.edit()
        editor!!.clear()
        editor.apply()
    }




}