package listadesuper.garagecoders.com.mx.listadesuper

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import listadesuper.garagecoders.com.mx.listadesuper.adapters.AdapterProducto
import listadesuper.garagecoders.com.mx.listadesuper.utils.Constants
import listadesuper.garagecoders.com.mx.listadesuper.utils.SharedListManagement
import android.app.Activity
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.widget.Toast
import mx.com.ia.comunicacioninterna.SharedPreferencesManager


class MainActivity : AppCompatActivity() {

    private var adapter: AdapterProducto =AdapterProducto(this)

    var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {

            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            val position = viewHolder.adapterPosition
            removeItem(position)
            SharedListManagement.deleteProduct(position)
            SharedPreferencesManager.saveProduct(applicationContext,SharedListManagement.getListProduct())
            Snackbar.make(viewHolder.itemView, "Se elimino el producto", Snackbar.LENGTH_LONG)
                    .setAction("Aceptar", null).show()
            updateRecycler()
            if(SharedListManagement.sizeListProduct()==0)
                visivilityEmptyMessage()
        }
    }

    fun updateRecycler(){
        this.adapter=AdapterProducto(this)
        adapter.update(SharedListManagement.getListProduct())
        this.recycler_view_productos.adapter=adapter

    }
    fun removeItem(position: Int){
        this.adapter.deleteItem(position)
    }

    fun visivilityEmptyMessage(){
        this.recycler_view_productos.visibility = View.INVISIBLE
        this.text_view_empty.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        SharedListManagement.veryficateProduct(SharedPreferencesManager.getProducts(this))
        if(!SharedListManagement.list.isEmpty()){
            adapter.update(SharedListManagement.list)
        }else {
            this.recycler_view_productos.visibility = View.INVISIBLE
            this.text_view_empty.visibility = View.VISIBLE
        }
        this.recycler_view_productos.adapter = adapter
        fab.setOnClickListener { view ->
            /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()*/
            val intent =Intent(this,AddProductActivity::class.java)
            startActivityForResult(intent,Constants.PICK_PRODUCT_REQUEST)
        }


        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(this.recycler_view_productos)

        this.srlContainer.setOnRefreshListener {
            this.adapter.update(SharedListManagement.getListProduct())
            this.srlContainer.isRefreshing = false
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (resultCode == Constants.PICK_PRODUCT_REQUEST) {
                this.adapter.update(SharedListManagement.list)
                this.text_view_empty.visibility=View.INVISIBLE
                this.recycler_view_productos.visibility=View.VISIBLE
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("","")
            }
    }




}
