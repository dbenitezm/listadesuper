package listadesuper.garagecoders.com.mx.listadesuper

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(true){
            val intent=Intent(this,MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}